FROM node:20-alpine AS build

WORKDIR /app

RUN npm install -g @angular/cli

COPY ./package.json .

RUN npm install

COPY . .

RUN ng build   

FROM nginx AS runtime

COPY --from=build /app/dist/angular-conduit /usr/share/nginx/html